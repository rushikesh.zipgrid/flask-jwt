import os
from consts import status
import tempfile

import pytest

from app import app, create_db
from models import db

USERNAME = "admin"
PASSWORD = "admin"


@pytest.fixture(scope="session", autouse=True)
def client():
    db_fd, app.config["DATABASE"] = tempfile.mkstemp()
    app.config["TESTING"] = True
    client = app.test_client()

    with app.app_context():
        db.drop_all()
        create_db()

    yield client

    os.close(db_fd)
    os.unlink(app.config["DATABASE"])


def test_registration(client):
    response = client.post(
        "/registration", json={"username": USERNAME, "password": PASSWORD}
    )
    assert response.status_code == status.HTTP_201_CREATED


def test_login(client):
    response = client.get("/login", data={"username": USERNAME, "password": PASSWORD})
    assert response.status_code == status.HTTP_200_OK
    assert "access_token" in response.json
    assert isinstance(response.json["access_token"], str)


def test_logout(client):
    response = client.get("/login", data={"username": USERNAME, "password": PASSWORD})
    access_token = response.json["access_token"]
    response = client.get(
        "/logout",
        headers={"Authorization": "Bearer {token}".format(token=access_token)},
    )
    assert response.status_code == status.HTTP_200_OK
    response = client.get(
        "/logout",
        headers={"Authorization": "Bearer {token}".format(token=access_token)},
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_resource(client):
    response = client.get("/login", data={"username": USERNAME, "password": PASSWORD})
    access_token = response.json["access_token"]
    response = client.get(
        "/resource",
        headers={"Authorization": "Bearer {token}".format(token=access_token)},
    )
    assert response.status_code == status.HTTP_200_OK
