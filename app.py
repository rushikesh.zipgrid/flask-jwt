from flask import Flask
from flask_restful import Api

from auth import jwt
from consts import config
import resources
from models import db

app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"] = config.SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = config.SQLALCHEMY_TRACK_MODIFICATIONS
app.config["JWT_SECRET_KEY"] = config.JWT_SECRET_KEY
app.config["JWT_BLACKLIST_ENABLED"] = config.JWT_BLACKLIST_ENABLED
app.config["JWT_BLACKLIST_TOKEN_CHECKS"] = config.JWT_BLACKLIST_TOKEN_CHECKS

jwt.init_app(app)
api = Api(app)
db.init_app(app)


@app.before_first_request
def create_db():
    db.create_all()


api.add_resource(resources.UserRegistration, "/registration")
api.add_resource(resources.UserLogin, "/login")
api.add_resource(resources.UserLogoutAccess, "/logout")
api.add_resource(resources.RestrictedResource, "/resource")
