from app import api
import resources

api.add_resource(resources.UserRegistration, "/registration")
api.add_resource(resources.UserLogin, "/login")
api.add_resource(resources.UserLogoutAccess, "/logout/access")
api.add_resource(resources.RestrictedResource, "/resource")

