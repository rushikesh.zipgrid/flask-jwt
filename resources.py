from flask_restful import Resource, reqparse, marshal_with
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity
import sqlalchemy
from werkzeug.exceptions import BadRequest

from auth import revoke_jwt
from models import UserModel
from consts import status, config
import serializers


argparser = reqparse.RequestParser()
argparser.add_argument("username", help="username required", required=True)
argparser.add_argument("password", help="password required", required=True)


class UserRegistration(Resource):
    def post(self):
        try:
            data = argparser.parse_args()
        except BadRequest:
            return {"message": "Invalid arguments"}, status.HTTP_400_BAD_REQUEST

        if not (data["username"] and data["password"]):
            return {"message": "Invalid arguments"}, status.HTTP_400_BAD_REQUEST

        user = UserModel(username=data["username"], password=data["password"])

        try:
            user.save_to_db()
            access_token = create_access_token(
                identity=user.id, expires_delta=config.JWT_TOKEN_EXPIRE_TIMEDELTA
            )
        except sqlalchemy.exc.IntegrityError:
            return {"message": "username already registered"}, status.HTTP_409_CONFLICT

        return (
            {
                "message": "user {0} created".format(data["username"]),
                "access_token": access_token,
            },
            status.HTTP_201_CREATED,
        )


class UserLogin(Resource):
    def get(self):
        try:
            data = argparser.parse_args()
        except BadRequest:
            return ({"message": "Invalid arguments"}, status.HTTP_400_BAD_REQUEST)
        current_user = UserModel.find_by_username(data["username"])

        if not current_user:
            return ({"message": "user doesn't exist"}, status.HTTP_401_UNAUTHORIZED)

        if current_user.verify_password(data["password"]):
            access_token = create_access_token(identity=current_user.id)
            return (
                {
                    "message": "user {0} logged in".format(current_user.username),
                    "access_token": access_token,
                },
                status.HTTP_200_OK,
            )
        else:
            return ({"error": "invalid credentials"}, status.HTTP_401_UNAUTHORIZED)


class UserLogoutAccess(Resource):
    @jwt_required
    def get(self):
        revoke_jwt()
        return {"message": "user logout"}


class RestrictedResource(Resource):
    @jwt_required
    def get(self):
        if UserModel.find_by_userid(get_jwt_identity()):
            return {"answer": 41}
        else:
            return {"message": "token invalid"}
