from passlib.hash import pbkdf2_sha256 as sha256
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, String

db = SQLAlchemy()


class UserModel(db.Model):

    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    username = Column(String(120), nullable=False, unique=True)
    password = Column(String(120), nullable=False)

    def save_to_db(self):
        # hash password before saving
        self.password = self.hash_password()
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_by_userid(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def get_all_users(cls):
        users = cls.query.all()
        return users

    @classmethod
    def delete_all_users(cls):
        num_rows_deleted = db.session.query(cls).delete()
        db.session.commit()
        return num_rows_deleted

    def verify_password(self, plain_text):
        return sha256.verify(plain_text, self.password)

    def hash_password(self):
        return sha256.hash(self.password)


class RevokedJWTTokenModel(db.Model):

    __tablename__ = "revoked_jwt_tokens"

    # Note: `user_id` is not foreign key because the user may be deleted after
    # the token has been created we are to make sure during insertion that
    # the id is a valid id
    seq = Column(Integer, primary_key=True)
    jti = Column(String(120), nullable=False)

    def add(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_token(cls, jwt_token):
        return cls.query.filter_by(jti=jwt_token).first()

    @classmethod
    def delete_all_tokens(cls):
        num_rows_deleted = db.session.query(cls).delete()
        db.session.commit()
        return num_rows_deleted
