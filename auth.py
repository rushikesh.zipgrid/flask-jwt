from flask_jwt_extended import JWTManager, get_raw_jwt

jwt = JWTManager()

from models import RevokedJWTTokenModel


@jwt.token_in_blacklist_loader
def check_if_token_blacklisted(decrypted_token):
    if RevokedJWTTokenModel.find_token(decrypted_token["jti"]) is not None:
        return True
    else:
        return False


def revoke_jwt():
    token = RevokedJWTTokenModel(jti=get_raw_jwt()["jti"])
    token.add()
