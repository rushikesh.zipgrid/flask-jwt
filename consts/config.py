from datetime import timedelta
from os import environ as env

JWT_SECRET_KEY = env["JWT_SECRET_KEY"]
SQLALCHEMY_DATABASE_URI = env["SQLALCHEMY_DATABASE_URI"]
SQLALCHEMY_TRACK_MODIFICATIONS = False
JWT_BLACKLIST_ENABLED = True
JWT_BLACKLIST_TOKEN_CHECKS = ["access", "refresh"]
JWT_TOKEN_EXPIRE_TIMEDELTA = timedelta(minutes=int(env["JWT_TOKEN_EXPIRY_MINUTES"]))  # token valid for 1 minute
