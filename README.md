# Flask JWT POC

This is project is POC of JWT using Flask.

## Getting Started

- Clone the repo
- Create virtual environment `$ python3 -m venv env`
- Activate virtual environment `$ source env/bin/activate`
- Install python modules `$ pip install -r requirements.txt`
- Create environment variables 
    ~~~
    $ export JWT_SECRET_KEY=<super-secret-key>
    $ export SQLALCHEMY_DATABASE_URI="sqlite:///<filename>.db"
    $ export JWT_TOKEN_EXPIRY_MINUTES=<preferred-minutes>
    ~~~
- Run application `$ flask run` 


## Running the tests
- Create environment variables 
    ~~~
    $ export JWT_SECRET_KEY=<super-secret-key>
    $ export SQLALCHEMY_DATABASE_URI="sqlite:///<test_filename>.db"
    $ export JWT_TOKEN_EXPIRY_MINUTES=<preferred-minutes>
    ~~~

- Run tests `$ pytest test.py`


## Format files

`$ black <filename>.py`

## Lint files

`$ pylint -disabled=R,C <filename>.py`

## Built With

* [Flask](https://flask.palletsprojects.com/en/1.1.x/) - The web framework
* [Flask SQLAlchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/) - ORM
* [Flask RESTful](https://flask-restful.readthedocs.io/en/latest/) - Extension for restful APIs
* [Passlib](https://passlib.readthedocs.io/en/stable/) - Password hashing library
* [PyJWT](https://pyjwt.readthedocs.io/en/latest/) - JWT library
* [Pytest](https://docs.pytest.org/en/latest/) - Test framework
* [PyLint](http://pylint.pycqa.org/en/latest/) - Linter
* [Black](https://black.readthedocs.io/en/stable/) - Formatter

## Authors

* **Rushikesh Patel**

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.
